import { Row, Col, Jumbotron } from 'react-bootstrap'
import BackButton from './BackButton'
import TravelButton from './TravelButton'
import { useRouter } from 'next/router'

export default function Banner({data}) {
    const router = useRouter()
    const {title, content} = data
    return (
        <Row>
            <Col>
                <Jumbotron>
                    <h1>{title}</h1>
                    <p>{content}</p>              
                    {router.pathname === '/' ? <TravelButton /> : <BackButton />}
                </Jumbotron>
            </Col>
        </Row>
    )
}

//conditional statement if '/' route show travel button, if not show back button