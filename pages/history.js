import { useEffect, useRef, useState, useContext } from 'react'
import { Table, Alert, Row, Col } from 'react-bootstrap'
import moment from 'moment'
import mapboxgl from 'mapbox-gl'
mapboxgl.accessToken = process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY
import Head from 'next/head'

export default function history(){
	const [records, setRecords] = useState([])
	const [longitude, setLongitude] = useState(0)
	const [latitude, setLatitude] = useState(0)

	const mapContainerRef = useRef(null)

	function setCoordinates(long, lat){
		setLongitude(long)
		setLatitude(lat)
	}

	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
			headers:{
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
	})
	.then(res => res.json())
    .then(data => {
        	if(data._id){
        		setRecords(data.travels)
        	} else {
        		setRecords([])
        	}
        })
	}, [])

	useEffect(() => {
		const map = new mapboxgl.Map({
			container: mapContainerRef.current,
			style: 'mapbox://styles/mapbox/streets-v11',
			center: [longitude, latitude],
			zoom: 12
		})

		const marker = new mapboxgl.Marker()
		.setLngLat([longitude, latitude])
		.addTo(map)

		map.addControl(new mapboxgl.NavigationControl(), 'bottom-right')

		return() => map.remove()
	}, [longitude, latitude])

	return(
		<React.Fragment>
		<Head>
			<title>Travel History</title>
		</Head>
		<Row>
			<Col xs={12} lg={8}>
				{records.length > 0
				?
				<Table striped bordered hover>
					<thead>
						<tr>
							<th>Origin</th>
							<th>Destination</th>
							<th>Date</th>
							<th>Distance (m)</th>
							<th>Duration (mins)</th>
							<th>Amount Paid (Php)</th>
						</tr>
					</thead>
					<tbody>
						{records.map(record => {
							return (
								<tr key={record._id}>
									<td onClick={() => setCoordinates(record.origin.longitude, record.origin.latitude)}>{record.origin.longitude}, {record.origin.latitude}</td>
									<td onClick={() => setCoordinates(record.destination.longitude, record.destination.latitude)}>{record.destination.longitude}, {record.destination.latitude}</td>
									<td>{moment(record.date).format('MMMM DD YYYY')}</td>
									<td>{Math.round(record.distance)}</td>
									<td>{Math.round(record.duration/60)}</td>
									<td>{record.charge.amount}</td>
								</tr>
							)
						})}
					</tbody>
				</Table>

				:
				<Alert variant="info">You have no travel records yet.</Alert>
				}
			</Col>
			<Col xs={12} lg={4}>
				<div className="mapContainer" ref={mapContainerRef}/>
			</Col>
		</Row>
		</React.Fragment>
	)
}

// look for nice node packages