import { useEffect, useState } from 'react'
import { Tabs, Tab } from 'react-bootstrap'
import moment from 'moment'
import MonthlyChart from '../components/MonthlyChart'

export default function insights(){

	const [distances, setDistances] = useState([])
	const [amounts, setAmounts] = useState([])
	const [durations, setDurations] = useState([])
	
	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data.travels.length > 0){
				let monthlyDistance = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				let monthlyAmount = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				let monthlyDuration = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				
				data.travels.forEach(travel => {
					const index = moment(travel.date).month()


					monthlyDistance[index] += (travel.distance/1000)
		// monthlyDistance[index] = monthlyDistance[index] + (travel.distance/1000)
					monthlyAmount[index] += (travel.charge.amount)
					monthlyDuration[index] += (parseInt(travel.duration/60))
					//parseint converst it to integer
				})

				setDurations(monthlyDuration)
				setAmounts(monthlyAmount)
				setDistances(monthlyDistance)
				console.log(monthlyDistance)
				console.log(monthlyAmount)
				console.log(monthlyDuration)
			}
		})
	}, [])

	return (
		<Tabs defaultActiveKey="distances" id="monthlyFigures">
			<Tab eventKey="distances" title="Monthly Distance Travelled">
				<MonthlyChart figuresArray={distances} label="Monthly Total in KMs"/>
			</Tab>
			<Tab eventKey="amount" title="Monthly Expenditures">
				<MonthlyChart figuresArray={amounts} label="Monthly Travel Expenses"/>
			</Tab>
			<Tab eventKey="duration" title="Time Spent Travelling/Month">
				<MonthlyChart figuresArray={durations} label="Time Spent Travelling/Month(Minutes)"/>
			</Tab>
		</Tabs>
	)
}