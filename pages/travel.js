import Head from 'next/head'
import dynamic from 'next/dynamic'

const DynamicComponent = dynamic(() => import('../components/StreetNavigation'))
//it is like turning the componentn into async, loads all components then applies the function (useEffect, etc.)
//reference: https://nextjs.org/docs/advanced-features/dynamic-import

export default function travel(){
	return(
		<React.Fragment>
		<Head>
			<title>Book a ride</title>
		</Head>
		<DynamicComponent />
		</React.Fragment>
	)
}